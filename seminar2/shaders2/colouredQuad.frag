#version 330
out vec3 color;

uniform vec4 viewport;
uniform mat3 colmatrix;
uniform float colorscale;

void main() {
    // Convert from screen space to texture space [0;1].
    vec2 pposition = (gl_FragCoord.xy - viewport.xy) / viewport.zw;
    // Convert to NDC space [-1;-1].
    pposition = pposition * 2 - 1;

    // Apply transformation matrix.
    vec3 transformed = (colmatrix * vec3(pposition.yx, 1)).yxz;
    // Perspective division leads.
    vec2 position = transformed.xy / transformed.z;

    // Just color magic.
    vec2 posNorm = abs(position) * colorscale;
    color = vec3(posNorm.xy, (position.x < 0 ? (position.y < 0 ? 0 : 0.25f) : (position.y < 0 ? 0.5f : 1.0f)));
}
